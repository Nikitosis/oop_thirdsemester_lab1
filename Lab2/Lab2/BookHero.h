#include "Hero.h"
#pragma once

/**
*@brief Class represents connection between book and hero. 
*Contains hero and his participation Level
*/
class BookHero
{
public:
	/**
	*@brief enum presents participation level of the hero
	*/
	enum ParticipationLevel {
		Main,
		Secondary,
		Episodic
	};

	/**
	*@brief converts ParticipationLevel to string
	*/
	static std::string participationLevelToString(ParticipationLevel participationLevel);

	BookHero(Hero& hero,ParticipationLevel participationLevel);
	~BookHero();

	Hero getHero() const;
	ParticipationLevel getParticipationLevel() const;

	bool operator==(const BookHero& bookHero) const;

private:
	Hero hero;
	ParticipationLevel participationLevel;
};

