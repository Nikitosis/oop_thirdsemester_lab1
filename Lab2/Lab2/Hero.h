#include <vector>
#include <string>
#pragma once

/**
*@brief class represents hero object.
*contains hero's names and pseudonames
*/
class Hero
{
public:
	Hero(std::vector<std::string> names);
	~Hero();

	std::vector<std::string > getNames();

	bool operator==(Hero& hero);
private:
	std::vector<std::string> names;
};

