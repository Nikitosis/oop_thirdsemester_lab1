#include "BookHero.h"


std::string BookHero::participationLevelToString(ParticipationLevel participationLevel)
{
	switch (participationLevel)
	{
	case 0: return "Main";
	case 1: return "Secondary";
	case 2: return "Episodic";
	default:
		return "None";
	}
}

BookHero::BookHero(Hero & hero, ParticipationLevel participationLevel):
	hero(hero),
	participationLevel(participationLevel)
{
}

BookHero::~BookHero()
{
}

Hero BookHero::getHero() const
{
	return hero;
}

BookHero::ParticipationLevel BookHero::getParticipationLevel() const
{
	return participationLevel;
}

bool BookHero::operator==(const BookHero& bookHero) const{
	return this->getHero().getNames() == bookHero.getHero().getNames() && this->participationLevel==bookHero.participationLevel;
}
