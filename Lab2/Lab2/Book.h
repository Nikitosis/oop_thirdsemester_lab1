#include <string>
#include <vector>
#include<ctime>
#include <iostream>

#include "BookHero.h"
#pragma once

/**
* @brief Class represents book object
*/
class Book
{
public:
	Book(std::string name, const std::vector<std::string > &authors);
	Book(std::string name, const std::vector<std::string > &authors, std::string issueDate,int pagesAm, std::string description);
	~Book();

	/**
	*@brief checks if two books belong to same series
	*@return true if two books belong to same series,otherwise false
	*/
	static bool areSameSeries(const Book& book1, const Book&book2);

	/**
	*@brief prints book info
	*/
	void print();

	/**
	*@brief adds hero to book
	*@param hero user wants to add
	*@param participationLevel of hero in this book
	*/
	void addBookHero(Hero hero, BookHero::ParticipationLevel participationLevel);

	bool operator<(const Book &book);
	
private:
	std::string name;
	std::vector<std::string > authors;
	std::string issueDate;
	int pagesAm;
	std::string description;
	std::vector<BookHero> bookHeroes;
};

