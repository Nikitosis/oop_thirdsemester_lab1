#define _CRT_SECURE_NO_WARNINGS

#include <iostream>
#include <string>
#include<vector>

#include "Series.h"
#include "Book.h"
#include "Hero.h"

using namespace std;

int main() {
	freopen("input.txt", "r", stdin);
	freopen("output.txt", "w", stdout);
	
	Series main;

	Hero colobok({"Kolobok","Pokatushkin","Eda"});
	Hero babaYaga({ "Baba Yaga","Babka","Staruha" });
	Book colobokBook("The Great Success Story of Colobok", { "Chaikovskiy" }, "2019-02-05", 20, "SomeDescription");
	colobokBook.addBookHero(colobok, BookHero::ParticipationLevel::Main);
	colobokBook.addBookHero(babaYaga, BookHero::ParticipationLevel::Secondary);

	main.addBook(colobokBook);

	Book babaYagaBook("Another book with 1 hero", { "MrWriter" }, "2019-01-05", 1005, "Description");
	babaYagaBook.addBookHero(babaYaga, BookHero::ParticipationLevel::Secondary);

	main.addBook(babaYagaBook);

	Book colobokAnother("Another book with 1 hero", { "MrWriter" }, "2019-01-05", 1005, "Description");
	colobokAnother.addBookHero(colobok, BookHero::ParticipationLevel::Main);

	main.addBook(colobokAnother);

	vector<Series> separateSeries = main.separateSeries();

	for (Series series : separateSeries) {
		series.printBooks();
		cout << endl;
		cout << "END OF SERIES" << endl;
		cout << endl;
	}
}