#include "Book.h"


Book::Book(std::string name,const std::vector<std::string> &authors, std::string issueDate, int pagesAm, std::string description):
	name(name),
	authors(authors),
	issueDate(issueDate),
	pagesAm(pagesAm),
	description(description)
{
}

Book::~Book()
{
}

bool Book::areSameSeries(const Book & book1, const Book & book2)
{
	for (int i = 0; i < book1.bookHeroes.size(); i++) {
		for (int j = 0; j < book2.bookHeroes.size(); j++)
			if (book1.bookHeroes[i] == book2.bookHeroes[j]) {
				if (book1.bookHeroes[i].getParticipationLevel() == BookHero::ParticipationLevel::Main ||
					book1.bookHeroes[i].getParticipationLevel() == BookHero::ParticipationLevel::Secondary)
					return true;
			}
	}
	return false;
}

void Book::print()
{
	std::cout << "-------------------"<<std::endl;
	std::cout << "Book name: " << name << std::endl;
	std::cout << "Authors: ";
	for (int i = 0; i < authors.size(); i++)
		std::cout << authors[i] << " ";
	std::cout << std::endl;
	std::cout << "Issue date: " << issueDate << std::endl;
	std::cout << "Pages amount: " << pagesAm << std::endl;
	std::cout << "Description: " << description<<std::endl;
	std::cout << "Book heroes: " << std::endl;
	for (BookHero bookHero : bookHeroes) {
		std::cout << "\tNames: ";
		for (std::string name : bookHero.getHero().getNames())
			std::cout << name << "/";
		std::cout << std::endl;
		std::cout << "\t\tParticipation level: " << BookHero::participationLevelToString(bookHero.getParticipationLevel()) << std::endl;
	}


	std::cout << "-------------------" << std::endl;

}

void Book::addBookHero(Hero hero, BookHero::ParticipationLevel participationLevel)
{
	bookHeroes.push_back(BookHero(hero, participationLevel));
}

bool Book::operator<(const Book&book) {
	return this->issueDate < book.issueDate;
}