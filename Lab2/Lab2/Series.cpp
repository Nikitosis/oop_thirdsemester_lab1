#include "Series.h"



Series::Series()
{
}


Series::~Series()
{
}

void Series::addBook(Book & book)
{
	books.push_back(book);
}

void Series::printBooks()
{
	std::sort(books.begin(), books.end());
	for (int i = 0; i < books.size(); i++)
		books[i].print();
}

std::vector<Series> Series::separateSeries()
{
	std::vector<Series> series;
	for (int i = 0; i < books.size(); i++) {
		for (int j = i + 1; j < books.size(); j++) {
			if (Book::areSameSeries(books[i], books[j]))
			{
				Series curSeries;
				curSeries.addBook(books[i]);
				curSeries.addBook(books[j]);
				series.push_back(curSeries);
			}
		}
	}
	return series;
}
