#include <vector>
#include <algorithm>

#include "Book.h"
#pragma once

/**
*@brief represents particular book group. 
*can be separated into series
*/
class Series
{
public:
	Series();
	~Series();

	/**
	*@brief adds book to series
	*/
	void addBook(Book& book);

	/**
	*@brief prints books
	*/
	void printBooks();

	/**
	*@brief separates books into series
	*@return vector of different series
	*/
	std::vector<Series> separateSeries();
private:
	std::vector<Book> books;
};

