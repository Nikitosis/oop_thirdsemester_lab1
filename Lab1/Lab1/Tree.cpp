#include "Tree.h"

Tree::Tree()
{
}

Tree::Tree(int nodesAm)
{
	graph.resize(nodesAm);
}

void Tree::addEdge(int fromNode, int toNode, int length)
{
	if (fromNode >= graph.size() || fromNode<0 || toNode>graph.size() || toNode < 0)
		throw std::invalid_argument("Node index is invalid");

	graph[fromNode].push_back(Edge(fromNode,toNode,length));
	graph[toNode].push_back(Edge(fromNode, toNode, length));
}

void Tree::removeEdge(int fromNode, int toNode)
{
	if (fromNode >= graph.size() || fromNode<0 || toNode>graph.size() || toNode < 0)
		throw std::invalid_argument("Node index is invalid");

	for(int i=0;i<graph.size();i++)
		for (int j = 0; j < graph[i].size(); j++)
		{
			if ((graph[i][j].fromNode == fromNode && graph[i][j].toNode == toNode) ||
				(graph[i][j].fromNode == toNode && graph[i][j].toNode == fromNode)) {
				graph[i].erase(graph[i].begin() + j);
				j--;
			}
		}
}

void Tree::addNode()
{
	graph.push_back({});
}

void Tree::print()
{
	for(int i=0;i<graph.size();i++)
		for (int j = 0; j < graph[i].size(); j++) {
			std::cout << graph[i][j].fromNode << " " << graph[i][j].toNode << " " << graph[i][j].length << std::endl;
		}
}

Tree Tree::getSkeletonTree()
{
	Tree newTree(graph.size());

	std::vector<Edge> edges;

	for (int i = 0; i < graph.size(); i++)
		for (int j = 0; j < graph[i].size(); j++)
			edges.push_back(graph[i][j]);

	std::vector<int> parents(graph.size());

	for (int i = 0; i < graph.size(); i++)
		parents[i] = i;

	std::sort(edges.begin(), edges.end());

	for(Edge edge:edges)
		if (getParent(edge.fromNode, parents) != getParent(edge.toNode, parents)) {
			newTree.addEdge(edge.fromNode, edge.toNode, edge.length);
			unite(edge.fromNode, edge.toNode, parents);
		}


	return newTree;
}

Tree::~Tree()
{
}

int Tree::getParent(int a,std::vector<int>& parents)
{
	if (a == parents[a])
		return a;
	return parents[a] = getParent(parents[a], parents);
}

void Tree::unite(int a, int b,std::vector<int>& parents)
{
	a = getParent(a, parents);
	b = getParent(b, parents);
	srand(time(0));
	if (rand() % 2 == 0)
		std::swap(a, b);
	parents[a] = b;
}

bool Tree::compareEdges(const Edge & a, const Edge & b)
{
	return a.length < b.length;
}

Tree::Edge::Edge(int fromNode, int toNode, int length):fromNode(fromNode),toNode(toNode),length(length)
{
}

bool Tree::Edge::operator<(const Edge & rhs)
{
	return this->length < rhs.length;
}
