#define _CRT_SECURE_NO_WARNINGS

#include <iostream>
#include <string>
#include<vector>

#include "Tree.h"

using namespace std;

int main() {
	freopen("input.txt", "r", stdin);
	freopen("output.txt", "w", stdout);
	
	Tree tree(10);
	tree.addEdge(1, 2, 10);
	tree.addEdge(1, 2, 5);
	tree.addEdge(1, 3, 15);
	tree.addEdge(2, 3, 2);
	tree.addEdge(4, 3, 18);
	tree.addEdge(4, 1, 10);

	tree.print();
	cout << endl;
	cout << endl;

	tree.getSkeletonTree().print();

}