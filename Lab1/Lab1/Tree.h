#include <vector>
#include <iostream>
#include <ctime>
#include <algorithm>

#pragma once
class Tree
{
public:
	Tree();
	Tree(int nodesAm);
	void addEdge(int fromNode, int toNode, int length);
	void removeEdge(int fromNode, int toNode);
	void addNode();
	void print();

	Tree getSkeletonTree();
	~Tree();

private:
	struct Edge {
		int fromNode;
		int toNode;
		int length;
		Edge(int fromNode, int toNode, int length);
		bool operator <(const Edge& rhs);
	};

	std::vector<std::vector<Edge>> graph;

	int getParent(int a,std::vector<int>& parents);
	void unite(int a, int b,std::vector<int>& parents);
	bool compareEdges(const Edge& a, const Edge& b);

};

